/*
      Programmcode für C02-Ampel

      for nodeMCU 1.0 Board
       Janis Rohrer
       Wolfgang Wolff
       Date: 26.03.2021 
       Version: 0.7
*/


#include "Seeed_BME280.h"
#include <MHZ.h>
#include <U8g2lib.h>
#include <ESP8266WiFi.h>
#include <SimpleKalmanFilter.h>
#include <time.h>
#include <Wire.h>

// pin definition
#define Pin_ROT D4
#define Pin_GRUN D3

#define CO2_IN 15
#define MH_Z19_RX D7
#define MH_Z19_TX D6

#define Addr 0x76
#define SDA_Pin 14
#define SCL_Pin 0

// defintion of objects 
U8G2_SH1106_128X64_NONAME_F_HW_I2C u8g2(U8G2_R0, U8X8_PIN_NONE);
MHZ co2(MH_Z19_RX, MH_Z19_TX, CO2_IN, MHZ19B);
BME280 bme280;
SimpleKalmanFilter kf = SimpleKalmanFilter(5, 5, 1);

// variables
unsigned long LastTime1 = 0;
unsigned long LastTime2 = 0;

int counter;
float co2Wert = 400;
float co2WertOld = 400;
bool InternetStatus;

// network 
const char* ssid = "physik";
const char* password = "penros31";

void setup() {
  Serial.begin(115200);

  pinMode(CO2_IN, INPUT);
  pinMode(Pin_ROT, OUTPUT);
  pinMode(Pin_GRUN, OUTPUT);

  u8g2.begin();
  configTime(2 * 3600, 0, "pool.ntp.org", "time.nist.gov");

  drawLogo();
  delay(5000);

  if (!bme280.init()) {
    Serial.println("No BME280 device found!");
  }

  while (co2.isPreHeating()) {
    drawLoadingSign();
    ToggleLED(counter);
    delay(500);
  }
}

void loop() {
  
  // every 1 second
  if ((millis() - LastTime1 ) > 1000) {

    LastTime1 = millis();
    float co2WertMessung = 1.0 * co2.readCO2PWM();
    co2Wert = kf.updateEstimate(co2WertMessung);
    Serial.println(String(co2Wert) + " " + String(co2WertMessung));
    
    if (abs(co2Wert - co2WertOld) > 1 || InternetStatus) {
      
      co2WertOld = co2Wert;
      ToggleLED(int((1.0 / 120) * sq(co2Wert / 100.0) + (7.0 / 60) * (co2Wert / 100.0)) * 2 + 1);
      drawDataSide(co2Wert, bme280.getTemperature(), bme280.getHumidity());
    }
  }

  // every 30 seconds
  if ((millis() - LastTime2) > 30000) {

    LastTime2 = millis();

    if (WiFi.status() != WL_CONNECTED) {
      WiFi.begin(ssid, password);
    } 
    else {
      InternetStatus = CheckInternet();
    }
  }
}

//---RGB_LED ansteuern und kontrollieren---//

void ToggleLED(int Zahl) {
  switch (Zahl) {
    case 0:
      RGB_Farbe(0, 0, 0);
      break;
    case 1:
      RGB_Farbe(0, 1, 0);
      break;
    case 2:
      RGB_Farbe(0, 0, 0);
      break;
    case 3:
      RGB_Farbe(1, 1, 0);
      break;
    case 4:
      RGB_Farbe(0, 0, 0);
      break;
    default:
      RGB_Farbe(1, 0, 0);
      break;
  }
}

void RGB_Farbe(int r, int g, int b) {
  digitalWrite(Pin_ROT, r);
  digitalWrite(Pin_GRUN, g);
}

bool CheckInternet(){
  /*
   * check wlan status
   */ 
  WiFiClient client;
  if(client.connect("www.google.com", 80)){
    client.stop();
    return true;
  }
  else{
    client.stop();
    return false;
  }
}


String getAktuelleTime() {
  /*
   * get time from wlan
   */
  time_t now = time(nullptr);
  struct tm* p_tm = localtime(&now);

  String Stunden = String(p_tm->tm_hour);
  String Minuten = String(p_tm->tm_min);
  String Sekunde = String(p_tm->tm_sec);

  if(Stunden.length() <= 1){
    Stunden = "0"+Stunden;
  }
  if(Minuten.length() <= 1){
    Minuten = "0"+Minuten;
  }
  if(Sekunde.length() <= 1){
    Sekunde = "0"+Sekunde;
  }
  return (Stunden + ":" + Minuten + ":" + Sekunde);
}
