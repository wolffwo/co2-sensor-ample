![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

# CO2 Senor Ampel für das Klassenzimmer 


---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Beschreibung der Hardware](#hardware)
- [Das PCB-Board](#pcb-board)
- [Die Firmware](#firmware)
- [Das Gehäuse](#gehäuse)
- [MQTT - Anbindung ans Schulnetzwerk](#mqtt)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Beschreibung der Hardware

## Das PCB-Board 

## Die Firmware

## Das Gehäuse

## MQTT - Anbindung ans Schulnetzwerk

## Troubleshooting
